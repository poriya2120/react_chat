import React,{useState,useEffect} from 'react';

import './App.css';
import Header from './components/header';
import Figure from './components/figure';
import WrongLetter from './components/WrongLetter';
import Word from './components/word';
const words = ['application', 'programming', 'interface', 'wizard'];

let selectedWord = words[Math.floor(Math.random() * words.length)];

let playable = true;


function App() {
  const [playable,setPlayable]=useState(true);
  const [correctLetters,setCorrectLetters]=useState([]);
  const [wrongLetters,setWrongLetters]=useState([]);
  useEffect(()=>{
const {key,keyCode}=event;
if (playable) {
  if (e.keyCode >= 65 && e.keyCode <= 90) {
    const letter = e.key.toLowerCase();

    if (selectedWord.includes(letter)) {
      if (!correctLetters.includes(letter)) {
        correctLetters.push(letter);

        displayWord();
      } else {
        // showNotification();
      }
    } else {
      if (!wrongLetters.includes(letter)) {
        wrongLetters.push(letter);

        updateWrongLettersEl();
      } else {
        showNotification();
      }
    }
  }
}
  });
  window.addEventListener('keydown', e => {
    if (playable) {
      if (e.keyCode >= 65 && e.keyCode <= 90) {
        const letter = e.key.toLowerCase();
  
        if (selectedWord.includes(letter)) {
          if (!correctLetters.includes(letter)) {
            correctLetters.push(letter);
  
            displayWord();
          } else {
            showNotification();
          }
        } else {
          if (!wrongLetters.includes(letter)) {
            wrongLetters.push(letter);
  
            updateWrongLettersEl();
          } else {
            showNotification();
          }
        }
      }
    }
  });
  return (
<>
    <Header/>
    <div className="game-container">
      <Figure/>
      <WrongLetter />
      <Word selectedWord={selectedWord} correctLetter={correctLetters}/>
    </div>
</>
  );
}

export default App;

